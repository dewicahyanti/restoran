const dataMilkshake = [
	{
		nama: 'Milkshake Strawberry',
		harga: 'Rp 25.000',
		gambar: 'asset/ms.webp'
	}, {
		nama: 'Milkshake Bluberry',
		harga: 'Rp 30.000',
		gambar: 'asset/blm.jpg'
	}, {
		nama: 'Milkshake Chocolate',
		harga: 'Rp 25.000',
		gambar: 'asset/cm.jpg'
	}, {
		nama: 'Milkshake Oreo',
		harga: 'Rp 25.000',
		gambar: 'asset/mo.jpg'
	},
];


const dataSmoothies = [
	{
		nama: 'Smoothies Strawberry-Pisang',
		harga: 'Rp 45.000',
		gambar: 'asset/sp.jpg'
	}, {
		nama: 'Smoothies Kiwi-Mangga',
		harga: 'Rp 45.000',
		gambar: 'asset/km.jpg'
	}, {
		nama: 'Smoothies Blueberry-Strawberry',
		harga: 'Rp 55.000',
		gambar: 'asset/sb.jpg'
	}, {
		nama: 'Smoothies Blueberry-Pisang',
		harga: 'Rp 50.000',
		gambar: 'asset/bp.jpg'
	},
];

const dataFrappe = [
	{
		nama: 'Frappucino Oreo',
		harga: 'Rp 35.000',
		gambar: 'asset/oreo.jpg'
	}, {
		nama: 'Frappucino chocolate',
		harga: 'Rp 35.000',
		gambar: 'asset/fc.jpg'
	}, {
		nama: 'Frappucino matcha',
		harga: 'Rp 50.000',
		gambar: 'asset/mf.jpg'
	}, {
		nama: 'Frappucino Strawberry',
		harga: 'Rp 50.000',
		gambar: 'asset/fs.jpg'
	},
];

//map
const loopMenu = (item, index, array) =>{
	const namaItem		= item.nama.toLowerCase();
	let namaContainer;

	if (namaItem.includes('milkshake')) 
		namaContainer = document.querySelector('.container-milkshake');

	else if(namaItem.includes('smoothies'))
		namaContainer = document.querySelector('.container-smoothies');

	else if (namaItem.includes('frapp'))
		namaContainer = document.querySelector('.container-frappe');

		namaContainer.innerHTML += `
	<div class="card card-promo">
             <div class="animate-img">
                 <img src="${item.gambar}" class="card-img-top img-fluid" alt="card1">
             </div>
             <div class="card-body">
                 <h5 class="card-title">${item.nama}</h5>
                 <p class="card-text">${item.harga}</p>
                <button class=" button-pesan btn btn-outline-dark">Pesan</button>
            </div>
        </div>
	`
}
dataMilkshake.map(loopMenu);
dataSmoothies.map(loopMenu);
dataFrappe.map(loopMenu);

//filter

const inputKeyword = document.querySelector('.input-keyword')
.addEventListener('input', (event) => {
	const keyword = event.target.value.toLowerCase();
	const containerMilkshake = document.querySelector('.container-milkshake')
	const cariMenu = (item, index)=>{
		return item.nama.toLowerCase().includes(keyword);
	}

	const hasilMilkshakes = dataMilkshake.filter(cariMenu);
	const hasilSmoothies  = dataSmoothies.filter(cariMenu);
	const hasilFrappes	  = dataFrappe.filter(cariMenu);

	document.querySelector('.container-milkshake').innerHTML = '';
	document.querySelector('.container-smoothies').innerHTML = '';
	document.querySelector('.container-frappe').innerHTML = '';

	hasilMilkshakes.map(loopMenu);
	hasilSmoothies.map(loopMenu);
	hasilFrappes.map(loopMenu);


});

//reduce

const jumlahPesanan = []
const buttonPesan = document.querySelectorAll('.button-pesan');
const elmntJumlahPesanan = document.querySelector('.jumlah-pesanan > p');

buttonPesan.forEach((item, index)=>{
item.addEventListener('click', ()=>{
	jumlahPesanan.push(1);
	
	
	const hasil = jumlahPesanan.reduce((accumulator, currentValue)=>{
		return accumulator + currentValue;
		}, 0);


	elmntJumlahPesanan.innerHTML = hasil;
	})
})